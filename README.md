# Python-Worm
A simple python script that uses sine waves to output a continuous wiggly line.

# Requirements:
* Python 3.9 or greater

## References:
* https://www.mathsisfun.com/algebra/amplitude-period-frequency-phase-shift.html
* https://www.asc.ohio-state.edu/price.566/courses/694/Sin_fun.htm
